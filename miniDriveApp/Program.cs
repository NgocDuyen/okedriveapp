﻿using Google.Apis.Drive.v3;
using miniDriveApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace miniDriveApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static async Task Main()
        {
            var drive = new GoogleDriveService();
            var identity =await drive.AuthorizeAsync(DriveService.Scope.Drive);
            var driveService =await drive.CreateDriveSevericeAsync(identity);

            var test = driveService.GetFolder();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
