﻿using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace miniDriveApp.Services
{
    public static class DriveServiceExtension
    {
        public static DriveList GetDrives(this DriveService driveService)
        {
            var request = driveService.Drives.List();
            return request.Execute();
        }
        public static FileList GetFolder(this DriveService driveService,string FolderId = null,string nextPage = null)
        {
            var request = driveService.Files.List();
            request.Q = "mimeType = 'application/vnd.google-apps.folder'";
            request.Fields = "*";
            if (!string.IsNullOrEmpty(nextPage))
            {
                request.PageToken = nextPage;
            }
            if (!string.IsNullOrEmpty(FolderId))
            {
                request.Q += $"and '{FolderId}' in parents";
            }
            else
            {
                request.Q += $"and 'root' in parents";
            }
            return request.Execute();
        }
    }
}
