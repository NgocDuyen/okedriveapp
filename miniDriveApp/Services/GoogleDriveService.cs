﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Drive.v3;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace miniDriveApp.Services
{
    public class GoogleDriveService
    {
        public GoogleDriveService()
        {
        }
        public async Task<UserCredential> AuthorizeAsync(params string[] Scopes)
        {
            var store = new FileDataStore("DriveProfile");
            await store.ClearAsync();

            return await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    new ClientSecrets
                    {
                        ClientId = "174888762231-hk2ls565afp1ac6ecr7cqi36r91b5fs1.apps.googleusercontent.com",
                        ClientSecret = "1C0O4D1hy_OsqxvKtmGctFz-"
                    },
                    Scopes,
                    Environment.UserName,
                    CancellationToken.None,
                    store);
        }
        public async Task<DriveService> CreateDriveSevericeAsync(UserCredential userCredential = null)
        {
            var Identity = userCredential == null ? await AuthorizeAsync(DriveService.Scope.Drive) : userCredential;
            return new DriveService(new Google.Apis.Services.BaseClientService.Initializer
            {
                HttpClientInitializer = Identity
            });
        }
    }
}
